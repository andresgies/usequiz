package com.usequiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsequizApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsequizApplication.class, args);
	}

}
