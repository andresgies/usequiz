package com.usequiz.web;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usequiz.domain.QuestionEntity;
import com.usequiz.service.ISaveAndList;

@RestController
@RequestMapping("/api")
public class QuestionResource {

	private final ISaveAndList<QuestionEntity> questionService;

	public QuestionResource(ISaveAndList<QuestionEntity> questionService) {
		this.questionService = questionService;
	}

	@GetMapping("/questions")
	public ResponseEntity<List<QuestionEntity>> getAllQuestions() {
		return ResponseEntity.ok(questionService.listAll());
	}

	@PostMapping("/questions/save")
	public ResponseEntity<String> saveQuestion(@RequestBody QuestionEntity question) {
		questionService.save(question);
		return ResponseEntity.ok().build();
	}
}
