package com.usequiz.service;

import java.util.List;

public interface ISaveAndList<T> {
	
	void save(T answer);
	
	List<T> listAll ();

}
