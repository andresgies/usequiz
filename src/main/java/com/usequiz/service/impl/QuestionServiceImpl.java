package com.usequiz.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.usequiz.domain.QuestionEntity;
import com.usequiz.repository.QuestionRepository;
import com.usequiz.service.ISaveAndList;

@Service
@Transactional
public class QuestionServiceImpl implements ISaveAndList<QuestionEntity> {

	private final QuestionRepository questionRepository;

	public QuestionServiceImpl(QuestionRepository questionRepository) {
		this.questionRepository = questionRepository;
	}

	@Override
	public void save(QuestionEntity question) {
		questionRepository.save(question);
	}

	@Override
	public List<QuestionEntity> listAll() {
		return questionRepository.findAll();
	}

}
